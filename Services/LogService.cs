﻿namespace Services
{
    public class LogService : ILogService
    {
        private const string _fileName = "persistence_storage.log";
        public LogService()
        {
        }

        public void Log(int num1, int num2, int sum)
        {
            using (StreamWriter sw = File.AppendText(_fileName))
            {
                sw.WriteLine($"num1:{num1},num2:{num2},sum:{sum}");
            }
        }
    }
}
