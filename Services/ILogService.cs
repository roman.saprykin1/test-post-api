﻿namespace Services
{
    public interface ILogService
    {
        public void Log(int num1, int num2, int sum);
    }
}
