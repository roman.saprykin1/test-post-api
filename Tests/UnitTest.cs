using Services;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestSum5()
        {
            var sumService = new TestService();
            var expected = 5;
            var result = sumService.Add(2, 3);
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void TestSum15()
        {
            var sumService = new TestService();
            var expected = 15;
            var result = sumService.Add(5, 10);
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void TestSumNegative()
        {
            var sumService = new TestService();
            var expected = -3;
            var result = sumService.Add(-5, 2);
            Assert.That(result, Is.EqualTo(expected));
        }
    }
}