using Microsoft.AspNetCore.Mvc;

using Data;
using Services;

namespace WebApp.Controllers
{
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly ITestService _testService;
        private readonly ILogService _logService;

        public TestController(ITestService testService, ILogService logService)
        {
            _testService = testService;
            _logService = logService;
        }

        [HttpPost("add")]
        public async Task<IActionResult> Sum(SumVM model)
        {
            var result = _testService.Add(model.Num1, model.Num2);
            _logService.Log(model.Num1, model.Num2, result);
            return Ok(result);
        }
    }
}