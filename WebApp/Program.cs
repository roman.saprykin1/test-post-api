using Services;
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
//configure services for DI
builder.Services.AddTransient<ITestService, TestService>();
builder.Services.AddTransient<ILogService, LogService>();

builder.Services.AddControllers();

builder.Services.AddCors(options =>
{
    options.AddPolicy(name: "FrontendUI",
                      policy =>
                      {
                          policy.WithOrigins("http://localhost:4200")
                          .AllowAnyMethod()
                          .AllowAnyHeader();
                                
                      });
});


var app = builder.Build();

app.UseHttpsRedirection();

app.UseAuthorization();
app.UseCors("FrontendUI");

app.MapControllers();

app.Run();
